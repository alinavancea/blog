---
layout: default
title: Blog
permalink: /blog/
---

<div class="home">
  <h1 class="page-heading">Blog</h1>
  <ul class="post-list">
    {% for post in site.categories["blog"] %}
      <li>
        <span class="post-meta">{{ post.date | date: "%b %-d, %Y" }}</span>
        <span class="post-meta">{{ post.categories | join: ', ' }}</span>
        <h2>
          <a class="post-link" href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a>
        </h2>
        <p>{{ post.intro }}<a href="{{ post.url }}">...</a></p>
      </li>
    {% endfor %}
  </ul>
  <p class="rss-subscribe">subscribe <a href="{{ "/feed.xml" | prepend: site.baseurl }}">via RSS</a></p>
</div>
