---
layout: page
title: About
permalink: /about/
---

I'm Alina, I'm software developer working remotly fro Romania.

Currently making [GitLab](https://gitlab.com/) magic ✨


This is a static website, generated with [Jekyll](https://jekyllrb.com/)

Hosted on GitLab free using [GitLab Pages](https://about.gitlab.com/product/pages/)

I write about my experience with Ruby, books I'm reading and working remotly.

Images I use are my own or created using [Canva](https://www.canva.com/)

I welcome and comment, note or feedback. Reach out on my [email](mailto:alina.vancea@gmail.com?subject=[Blog]%20Implementing%20Coffee%20Chat%20Bot%20for%20RocketChat%20in%20Ruby)

Stay safe, healty and be kind!

💜Alina!