---
layout: post
title: Ruby under a microscope - How ruby executes your code
date:   2020-12-07
categories: [books, ruby]
author: Alina
intro: Ruby core tea design YARV to use a stack pointer and a program counter.
---

Ruby core tea design YARV to use a stack pointer and a program counter.

### YARV's internal stack and your ruby stack

YARV uses a stack internally to track intermediate values, arguments and return values. YARV is a stack-priented virtual machine. 

YARV keeps track of your ruby program's call stack, recording which methods call which other methods, functions, blocks and so on.
YARV is a double-stack machine, keeps track of internal instruction but also for your ruby program.

### Notes

The main difference between stack pointer and program counter is that the stack pointer is a register that stores the address of the last program request in a stack while the program counter is a register that stores the address of the next instruction to be executed from the memory

A method, like a function, is a set of instructions that perform a task. The difference is that a method is associated with an object, while a function is not. In ruby all functions are in fact methods associated with a class, there is always a receiver. Ruby’s parser and compiler distinguish between functions and methods: Method calls have an explicit receiver, while function calls assume the receiver is the current value of self.
