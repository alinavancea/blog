---
layout: post
title:  Retrospective 2020
date:   2020-12-30 
categories: [blog]
author: Alina
intro: For about 6 years together with my husband we are doing at the end of the year a retrospective. We discuss about what we achieved the current year together and individually.
---

For about 6 years together with my husband we are doing at the end of the year a retrospective. We discuss about what we achieved the current year together and individually.
 
It has been a tough year for everyone, our routines have changed, we are tired.
 
Our retrospective is about how we see from a high level the full year and what we want for the next one.
 
We did not have a structure from the beginning, most of the time our retrospective is a discussion. I would have liked to have it written down since we have started doing this.
 
### What went well?
 
* I changed job as we planned, started to work for [GitLab](https://gitlab.com) since February 2o2o, still here.
* Husband quit his job and works full time on his project.
* Paid our debt.
* Both worked from home, I had company this year.
* Keep ourselves healthy.
* Helped my parents with renovating their home.    
* [Worked 2 weeks from seaside](https://alinamihaila.ro/blog/2020/09/29/working-2-weeks-at-sea-side.html). 
 
### What we could have done better?

* Reading, I had another plan with reading this year.
* Being active, with the restrictions and lockdowns we stayed so much time inside, I don't find my motivation to workout or go out for walks.
 
### What we learned?

* I've learned to be grateful for working remotely and have a job in this strange times.
* We can live with less things
* We can do plans and radically change and adjust.
* Living in a small city has advantages, infections with Covid were less here.
* I miss gym, gym is an important piece in my daily routine.
 
### What do we focus on next year?

* Don't get Covid, get the vaccine. 
* Prioritize fitness. 
* Travel somewhere sunny and warm.
* Study and learn to level up my programming skills. 
* Create more, consume less.
* Work from another place, seaside and maybe somewhere in the mountains.
* Continue working on the retiring at 35 plan.
 
This year flew away so fast, it feels like I didn’t do so much and I did a lot at the same time. 
 
Are you doing a end of year retrospective? What your retrospective looks like?

I wish you a Happy New Year!


