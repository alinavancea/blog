---
layout: post
title: Contributing to open source for the first time
date:   2021-10-17
categories: [blog, ruby]
author: Alina
intro: This is my first official contribution to an open source project and makes me happy.
image: /assets/images/posts/2021-10-17/computer.png
---

I’ve mentioned in a [previous post](2021-03-28-implementing-a-coffee-chat-bot-for-rocketchat-in-ruby.md) that I’m using successfully  for a couple of projects the [rocket-chat gem](https://github.com/abrom/rocketchat-ruby). 

For one of the projects I need the list of members for a given channel. This was not available in the API. For the time being I decided to proceed further by adding my own method by inheriting Rocket::Channel class. All worked fine and the coffee-chat was what we wanted.
Back then I mentioned the opened [issue](https://github.com/abrom/rocketchat-ruby/issues/33) to add this method to the rocket-chat gem. 

I simply opened an [issue](https://github.com/abrom/rocketchat-ruby/issues/33) and a pull request. Andrew was very responsive and kind, giving me the direction on implementation. 

Now this new functionality is available for anyone to use.

The code for the coffee-chat bot become event more simple.

There is one ruby script for the logic we need.

```ruby
require 'rocketchat'

ROCKET_LOGIN    = ARGV[0]
ROCKET_PASSWORD = ARGV[1]
ROCKET_CHANNEL  = ARGV[2]
ROCKET_URL      = ARGV[3] 
FALLBACK_USER   = ARGV[4] 

def coffee_message(member)
  "Hello! It's time for a coffee-chat! You are select to have a coffee-chat with @#{member.username}. Get in touch to find the best timing"
end

# Send a private message to both members
def match_with_message(session, member1, member2)
  if member1 && member2
    session.chat.post_message(room_id: member1.id, text: coffee_message(member2))
    session.chat.post_message(room_id: member2.id, text: coffee_message(member1))
  end
end

rocket_server = RocketChat::Server.new(ROCKET_URL)

session = rocket_server.login(ROCKET_LOGIN, ROCKET_PASSWORD)

members = []
offset = 0
count = 50

loop do
  batch = session.channels.members(name: ROCKET_CHANNEL, offset: offset, count: count)
  members.concat(batch)

  break if batch.size < 50

  offset += count
end

# Remove the bot user
members.reject!{ |member| member.username == ROCKET_LOGIN}

# Remove the fallback user in case of odd number of mebers 
# in order to avaoid matching with nil
if members.size.odd? 
  members.reject!{ |member| member.username == FALLBACK_USER }
end

shuffled = members.shuffle.each_slice(2).to_a

shuffled.each do |first, second|
  match_with_message(session, first, second)
end

session.chat.post_message(room_id: ROCKET_CHANNEL, text: "Au fost programate #{shuffled.size} coffee-chats in aceasta saptamana")

```

Honestly this code could be improved, and I have a couple of ideas on how this could look like. The important part is that it is working and fixing the problem. It has been working for more than 6 months.
 
There is always a balance I'm trying to keep when writing code, I'm trying to answer to a couple of questions before investing more time and energy:
 
- Does it fix the problem?
- Is the code working?
- Does it require maintenance?
- Are there new requirements?
- Do I have to make changes often?
 
## Contributing to open source projects
 
Here are some ideas on how we can contribute:
 
- Find an open source project to contribute on.
- Read the project guide, generally they have a guide on how to start contributing.
- Look for issues marked as contribution, or first issue, community usually marks this issues in order to be easy to spot.
- Add a comment and a note to state your intention.
- Open a pull request(merge request) and add comments, explanations, this will help refers to understand with expressing your intentions.
 
This is my first official contribution to an open source project and makes me happy.

🐞Stay safe and be kind!🐞
💜Alina
