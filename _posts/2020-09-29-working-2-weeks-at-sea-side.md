---
layout: post
title: Working 2 weeks from seaside
date:   2020-09-29
categories: [blog]
author: Alina
intro: At the beginning of September we had the crazy idea to stay 2 weeks at Black Sea side. It is off sezon, it shouldn't be crowded, might be a bit windy and rainy but we get to see the sea this year.
---
 
### Working remotely
 
We all have a weird year, 2020 was and still is weird with everything it is happening around the world.
 
We had a one week holiday in June when we went in Siret Delta for a couple of days, feeling quite stressed about the environment and about the coronavirus.
 
At the beginning of September we had the crazy idea to stay 2 weeks at Black Sea side. It is off sezon, it shouldn't be crowded, might be a bit windy and rainy but we get to see the sea this year.
 
We had an apartment with a sea view that was perfect. Having coffee with this view is magic.
 
### Preparation
 
- I took with me a second screen, I knew I needed it in order to be able to work.
- I tried to finish the complex tasks I had before the trip.
- We had set up the kitchen table as the working(office) area, so we have a separation of work and other activities.
- Get in place a new routine that fits the activities we want to do.
 
### Thoughts
 
First days I had my mind all over the place. Should I work now? Should I go to the beach? When working my thoughts were away, when I was at the beach my thoughts were at work.
 
Slowly we got a ritm and had a schedule, 2 walks a day in the morning and in the evening, one hour sun break  before lunch.
 
It was a great experience, we want to do this again in spring time, I definitely needed an escape from home and doing the same thing every day.
 
We are fortunate and grateful to be able to work from anywhere.
 
Looking to go somewhere close during the winter.
 
Stay safe 🐞
 
![Coffee at sea side](/assets/images/posts/2020-09-29-coffee.jpg)
 
 

