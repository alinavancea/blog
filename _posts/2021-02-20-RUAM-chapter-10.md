---
layout: post
title: Ruby under a microscope - Ruby on the JVM
date: 2021-02-20
categories: [books, ruby]
author: Alina
intro: JRuby is an alternative implementation for Ruby, in Java instead of C. Java allows Ruby application to run like any other Java program using the Java Virtual Machine and allows to use thousands of libraries written in Java
---

JRuby is an alternative implementation for Ruby, in Java instead of C. Java allows Ruby application to run like any other Java program using the Java Virtual Machine and allows to use thousands of libraries written in Java

### Running Programs with MRI and JRuby

`ruby block.rb` launches a binary executable, the product of compiling Ruby’s C source code during the build process

`jruby block.rb` jruby command doesn’t map to a binary executable, it refers to a shell script that executes the java command.  You launch a binary executable the JVM which executes the JRuby Java application. This application in turn parses compiles and executes your Ruby script while running inside JVM

#### How JRuby  Parses and compiles the code

Once we launch the JRuby it needs to parse and compile the code. It uses a parser generator just as MRI uses Bison. JRuby uses a parser generator called JAy during the build process to create the code that will parse your Ruby code. Jay is similar to Bison except it is written in Java instead of C. Instead of YARV instructions it produces Java bytecode instruction

#### How JRuby executes your code

JRuby tokenizes and parses the code almost the same way that MRI does. MRI and JRuby use two very different virtual machines to execute the code.

The ability to use JVM is important for two reasons:

Environmental It alloes to use Ruby on servers, in applications and in IT organizations where you could not run Ruby before

Technical The JVM is the product of alsmos 20 years of intense reasoerach and development. Resolves many difficult computer science problems, like garbage collecting and multithreading. Ruby can often run faster and more reliably on the JVM.
 
#### Implementing Ruby classes with Java clases 
Java supports writing classes creating objects, JRuby is also object oriented. JRuby uses Java objects instead of C structures. Java we can use inheritance which might simplify  internal implementation.
