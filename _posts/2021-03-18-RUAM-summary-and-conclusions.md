---
layout: post
title: Ruby under a miscroscope - Thoughts and Conclusions
date: 2021-03-18
categories: [books, ruby]
author: Alina
intro: There is value in knowing where to look when you want to understand what is happening behind the scenes when using a programming language.
---

I find it useful to read and learn about how a language works, especially for the language I'm using to write code almost on a daily basis. 

To be honest for me this book was harder to read, it gets into depth of the Ruby language and its complexity. Has often references to C code implementations which brings back high school and university memories. 

I’m lucky that at GitLab we read this book under a Book Club, which kept me on track and more than that we had the chance to discuss and clarify different aspects from the book. 

There is value in knowing where to look when you want to understand what is happening behind the scenes when using a programming language. 

For me this is the kind of book I'll return when I need more details or when I don't understand what is happening in upper layers.

I’m probably not going to write a language, but I know where to look if I decide too.



