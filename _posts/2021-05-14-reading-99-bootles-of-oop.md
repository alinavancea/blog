---
layout: post
title: 99 Bottles of OOP - Takeways 
date:   2021-05-15
categories: [blog, books, ruby]
author: Alina
intro: Writing code that is easy to read and understand is an act of kindensss
---

As developers we spend more time to read code that to write it. Writing code that is easy to read and understand is an act of kindensss.

The win of the week! We have finished the [**99 bottles of OOP book** by Sandy Metz, Katrina Owen, TJ Stankus](https://sandimetz.com/99bottles) in the GitLab book Club. 

There is a real value in participating in a book club. We have the chance to discuss the topics in the book, share opinions, 
agree or disagree, ask questions, get clarifications. Besides this, there is a sweet group pressure to read the book before 
the weekly meeting to discuss the chapters. 

### Takeaways  from this book

#### The naming framework.

> "There are only two hard things in Computer Science: cache invalidation and naming things." - Phil Karlton

I often have trouble finding good names, in this book they provide a framework for naming things, which I'm already using and 
I hope to get in a place where this would become easier for me.

The name of a thing should be one level of abstraction higher than the thing itself.

Example for naming a variable:

| Number | xxx?(to be named) |
| ------ | ----------------- |
| 1      | bottle              |
| 6      | six-pack         |
| n      | bottles           |
| n      | carafe             |
| n      | cans               |
| n      | container       |

The resulting variable name will be `container`.

#### Flocking rules

The flocking rules are a set of rules for refactoring code. The idea is to make small incremental changes. This allows making complex 
changes in small repeated steps.

1. Select the things that are most alike.
2. Find the smallest difference between them.
3. Make the simplest change that will remove that difference

#### Make the easy change

> "Make the change easy (warning: this may be hard), then make the easy change" - Kent Beck

Looking every day at code, there are often occasions to improve it. How to approach refactoring, how to add a new feature under existing code? 

The recommendation is to first refactor the code in such a way it is open for extension. This might be the hard change that pays of the following easy change.

#### Take time to identify the code smells

This is a habit I want to build next. It doesn't have to end up always with a refactor on what I notice but I think would be a great practice 
to take the time to identify the code smell I notice on my day-to-day job and try to make an exercise on what refactoring should look like. 
The [Refactoring Guru](https://refactoring.guru) is a great resource to find the recipe for the code smell.

The book is about writing good code, how to solve a problem, building a routine. Always apply the same rules, the same process would 
finally lead to a similar code.

For me, the next step is to get one small habit in place, to notice the code smells and look for the corresponding recipe 
and pick another small habit after.

🐞Stay safe and be kind!🐞
💜Alina


