---
layout: post
title: Ruby under a microscope - The hash table the workhorse  of Ruby internals
date: 2021-01-31
categories: [books, ruby]
author: Alina
intro: Unlike simple arrays a hash table can automatically expand to accommodate more values, no need to worry about the space or about allocating memory.
---

Unlike simple arrays a hash table can automatically expand to accommodate more values, no need to worry about the space or about allocating memory.

Among other things Ruby uses a hash table to hold the hash objects created. Every time we create a method, a constant,  instance variables for generic objects like integers strings
 
Hash tables organize values in groups or bins, based on an integer value calculated from each value - a hash. When we need to find a value we can figure out in which bin it’s in by recalculating its hash value and speed up the search.

RHash structure, short for Ruby hash. This has a st_table structure which contains the basic information about the hash, including the number of entries, the number of bins, a pointer to the bins. 
