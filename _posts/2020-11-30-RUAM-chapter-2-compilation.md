---
layout: post
title: Ruby under a microscope - Compilation
date:   2020-11-30
categories: [books, ruby]
intro: Compilation means translating code from one programming language to another. Your programming language is easy for you to understand while the target language is easy for computers to understand. Ruby's compiler runs automatically without you ever knowing.
---
 
*Compilation* means translating code from one programming language to another. Your programming language is easy for you to understand while the target language is easy for computers to understand. Ruby's compiler runs automatically without you ever knowing.
 
The Ruby core team introduced a compiler with version 1.9. Earlier versions don't contain a compiler. Ruby 1.8 immediately executes your code after tokenizing and parsing are finished. This is done by going through the nodes in the AST tree and executing each one.
 
With Ruby 1.9 it was introduced YARV(Yet Another Ruby Virtual Machine) which actually executes your Ruby code, the same idea as Java Virtual Machine.
 
When using YARV, you first compile your code into bytecode, low-level instructions that the virtual machine understands. Ruby doesn't expose the compiler as a separate tool, it automatically compiles Ruby code into bytecode instructions internally.
Ruby never compiles the Ruby code all the way to machine language. Compiles the code into a series of low-level instructions called YARV instructions. 
 
YARV is a stack orientated virtual machine, it maintains a stack of values, mainly arguments and returns values for the YARV instructions.
 
Ruby’s compiler works by iterating through AST produced by the tokenizing and parsing processes, generating bytecode instructions on the way. Ruby translates the code from Ruby to YARV instructions. Every block, method, or scope in a program has a corresponding set of bytecode instructions. 


